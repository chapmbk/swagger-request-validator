/**
 * This example shows how to add the adapter to the swagger-request-validator in the most simple way.
 *
 * @see com.atlassian.oai.validator.springmvc.example.simple.RestRequestValidationConfig
 */
package com.atlassian.oai.validator.springmvc.example.simple;